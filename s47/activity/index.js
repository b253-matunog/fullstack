// alert("Hello!");

// Access the elements in the the html
const firstName = document.querySelector("#txt-first-name");
const lastName = document.querySelector("#txt-last-name");
const fullName = document.querySelector("#full-name");

// function to print the full name
function printFullName () {

  fullName.innerHTML = `${firstName.value} ${lastName.value}`; // you can also use textContent instead of innerHTML
};


firstName.addEventListener("keyup", printFullName);
lastName.addEventListener("keyup", printFullName);